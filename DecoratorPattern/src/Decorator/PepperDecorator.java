package Decorator;

import Component.IPizza;

public class PepperDecorator extends PizzaDecorator {

	public PepperDecorator(IPizza pizza) {
		super(pizza);
	}

	@Override
	public String doPizza() {
		return this.pizza.doPizza() + " add pepper";
	}

}
