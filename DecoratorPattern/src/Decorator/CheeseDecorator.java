package Decorator;

import Component.IPizza;

public class CheeseDecorator extends PizzaDecorator {

	public CheeseDecorator(IPizza pizza) {
		super(pizza);
	}

	@Override
	public String doPizza() {
		return this.pizza.doPizza() + " add cheese";
	}

}
