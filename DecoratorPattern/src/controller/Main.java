package controller;

import Component.ChickenPizza;
import Component.IPizza;
import Component.TomatoPizza;
import Decorator.CheeseDecorator;
import Decorator.PepperDecorator;

public class Main {
	public static void main(String[] args) {
		IPizza ck_Pizza = new ChickenPizza();
		IPizza tm_Pizza = new TomatoPizza();
		// ONLY chiken pizza
		System.out.println(ck_Pizza.doPizza());
		// ONLY tomato pizza
		System.out.println(tm_Pizza.doPizza());
		
		PepperDecorator pepperDecorator = new PepperDecorator(ck_Pizza);
		// Chicken pizza + pepper
		System.out.println(pepperDecorator.doPizza());
		// Chicken pizza + pepper + cheese
		CheeseDecorator cheeseDecorator = new CheeseDecorator(pepperDecorator);
		System.out.println(cheeseDecorator.doPizza());
	}
}
